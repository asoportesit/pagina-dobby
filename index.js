function informationFact(){
    document.getElementById("informationDetail").innerHTML ="Cambie su facturero físico por uno digital y emita sus comprobantes desde cualquier dispositivo y lugar"
}

function informationInv(){
    document.getElementById("informationDetail").innerHTML ="Tenga el control de su inventario y obtenga reportes automáticos en tiempo real"
}

function informationPos(){
    document.getElementById("informationDetail").innerHTML ="Punto de Venta Dobby la solución ideal para restaurantes"
}


//:::::::
document.addEventListener('DOMContentLoaded', function(){
    
    const modal_container = document.getElementById("modal_container");

    if(modal_container){
        
        const close = document.getElementById("close");
        modal_container.classList.add("show");
    
        close.addEventListener("click", () => {
            modal_container.classList.remove("show");
        });
    }
    
    window.addEventListener('scroll', stickyNav);
    let navbar = document.getElementById("navbar");

    let sticky = navbar.offsetTop;

    function stickyNav(){
        if(window.pageYOffset >= sticky){
            navbar.classList.add("sticky");
        }else{
            navbar.classList.remove("sticky");
        }

    }

    // 
    const formularioContacto = document.getElementById('formularioContacto');
    
    if(formularioContacto){
        formularioContacto.addEventListener("submit", async (e)=>{
            e.preventDefault();
            try {
                const name = document.querySelector("input[name='name']");
                const email = document.querySelector("input[name='email']");
                const message = document.querySelector("textarea[name='message']");
                const phone = document.querySelector("input[name='phone']");
                
                
                const body = {
                    name: name.value,
                    email: email.value,
                    message: message.value,
                    phone: phone.value
                }
        
                const peticion = await fetch("http://localhost:3000/send-email", { 
                
                    method: "POST",
                    body: JSON.stringify(body),
                    headers: {"Content-Type":"application/json" }
                });

                let msm = document.getElementById("msmSubmit")
                msm.style.display = "block"
                setTimeout(()=> { 
                    msm.style.display = "none"
                }, 3000);
                formularioContacto.reset();

            } catch (error) {
                console.error(error);
                let msm = document.getElementById("error")
                msm.style.display = "block"
                setTimeout(()=> { 
                    msm.style.display = "none"
                }, 3000);
                formularioContacto.reset();

                
            }
        })
    }

    // Cotizar
    const formularioCotizar = document.getElementById('formularioCotizar');
    
    if(formularioCotizar){
        formularioCotizar.addEventListener("submit", async (e)=>{
            e.preventDefault();
            try {
                const name = document.querySelector("input[name='name']");
                const number = document.querySelector("input[name='number']");
                const email = document.querySelector("input[name='email']");
                const whoIs = document.querySelector("input[name='whoIs']");
                
                const body = {
                    name: name.value,
                    number: number.value,
                    email: email.value,
                    whoIs: whoIs.value
                }
        
                const peticion = await fetch("http://localhost:3000/send-quotation", { 
                
                    method: "POST",
                    body: JSON.stringify(body),
                    headers: {"Content-Type":"application/json" }
                });
                
                let alert = document.getElementById("Submit");
                alert.style.display="block";
                setTimeout(() => {
                    alert.style.display="none"
                }, 3000);
                formularioCotizar.reset();
                
            } catch (error) {
                console.error(error);
                let err = document.getElementById("err");
                err.style.display="block";
                setTimeout(() => {
                    err.style.display="none"
                }, 3000);
                formularioCotizar.reset();


                
            }
        });
    }

});

// 
function validarPhone(phone){
    if(phone.length <= 10){
        return phone
    }else{
        console.log("error");
    }
    

}



